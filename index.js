const { promisify } = require('util')

const OVERSIZE_REGEX = /^Encoded message must not be larger than 8192 bytes/

module.exports = function SplitPublish (ssb, splitter, opts = {}) {
  //
  // content --splitter--+--> firstChunk  ---publish--------> firstMsg
  //                     |--> secondChunk ---splitPubish ---> [msgs]
  //
  // - when "firstChunk" is published, the associated message is firstMsg
  // - if this function recurses, first/ second is relative to the current scope

  const publish = (content, cb) => {
    ssb.publish(content, (err, msg) => {
      if (err) return cb(err)
      if (typeof msg.value.content === 'object') return cb(null, msg)

      ssb.get({ id: msg.key, private: true, meta: true }, cb)
      // NOTE there's an edge case where author cannot decrypt the message they have sent, but we'll ignore that for now
    })
  }

  return function splitPublish (content, cb) {
    if (cb === undefined) return promisify(splitPublish)(content)

    publish(content, (err, msg) => {
      if (err) {
        if (err.message.match(OVERSIZE_REGEX)) {
          let [firstChunk, secondChunk] = splitter(content)

          publish(firstChunk, (err, firstMsg) => {
            if (err) return cb(err) // "tried to split, still failed"

            if (opts.afterPublish) secondChunk = opts.afterPublish(firstMsg, secondChunk)

            splitPublish(secondChunk, (err, msgs) => {
              if (err) return cb(err) // awkward, already published >= 1

              cb(null, [firstMsg, ...msgs])
            })
          })
        } // eslint-disable-line
        else cb(err)
      } // eslint-disable-line
      else cb(null, [msg])
    })
  }
}
