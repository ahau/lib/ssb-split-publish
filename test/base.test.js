const test = require('tape')

const SplitPublish = require('../')
const Server = require('./testbot')

function splitter (content) {
  const first = {
    type: 'post',
    text: content.text.slice(0, 4000), // janky!
    root: content.root || null
  }
  if (content.branch) first.branch = content.branch

  const second = {
    type: 'post',
    text: content.text.slice(4000), // janky!
    root: content.root
  }

  return [first, second]
}

test('splitter (small message)', async t => {
  const ssb = Server()

  const publish = SplitPublish(ssb, splitter)

  const smallContent = {
    type: 'post',
    text: 'hooray!',
    root: null
  }

  await new Promise((resolve) => {
    publish(smallContent, (err, msgs) => {
      t.error(err, 'publishes small message')

      t.deepLooseEqual(
        msgs.map(m => m.value.content),
        [smallContent],
        'returns an array'
      )

      resolve()
    })
  })

  const msgs = await publish(smallContent)
  t.deepLooseEqual(
    msgs.map(m => m.value.content),
    [smallContent],
    'can be used as a promise'
  )

  t.end()
  ssb.close()
})

test('splitter (large message)', async t => {
  const ssb = Server()

  const publish = SplitPublish(ssb, splitter)

  const largeContent = {
    type: 'post',
    text: [
      ...new Array(4000).fill('a'),
      ...new Array(5000).fill('b'),
      ...new Array(4000).fill('c')
    ].join(''),
    root: null
  }

  await new Promise((resolve) => {
    publish(largeContent, (err, msgs) => {
      if (err) throw err
      t.error(err, 'publishes messages')

      const expected = [
        {
          type: 'post',
          text: new Array(4000).fill('a').join(''),
          root: null
        },
        {
          type: 'post',
          text: new Array(4000).fill('b').join(''),
          root: null
        },
        {
          type: 'post',
          text: [
            ...new Array(1000).fill('b'),
            ...new Array(4000).fill('c')
          ].join(''),
          root: null
        }
      ]

      t.deepLooseEqual(
        msgs.map(m => m.value.content),
        expected,
        'returns an array'
      )

      resolve()
    })
  })

  const msgs = await publish(largeContent)
  const expected = [
    {
      type: 'post',
      text: new Array(4000).fill('a').join(''),
      root: null
    },
    {
      type: 'post',
      text: new Array(4000).fill('b').join(''),
      root: null
    },
    {
      type: 'post',
      text: [
        ...new Array(1000).fill('b'),
        ...new Array(4000).fill('c')
      ].join(''),
      root: null
    }
  ]
  t.deepLooseEqual(
    msgs.map(m => m.value.content),
    expected,
    'can be used as a promise'
  )

  t.end()
  ssb.close()
})
