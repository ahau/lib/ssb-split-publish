const test = require('tape')
const pull = require('pull-stream')

const SplitPublish = require('../')
const Server = require('./testbot')

function splitter (content) {
  const first = {
    type: 'post',
    text: content.text.slice(0, 4000), // janky!
    root: content.root || null
  }
  if (content.branch) first.branch = content.branch

  const second = {
    type: 'post',
    text: content.text.slice(4000), // janky!
    root: content.root,
    branch: ['TODO']
  }

  if (content.recps) {
    first.recps = content.recps
    second.recps = content.recps
  }

  return [first, second]
}

function afterPublish (firstMsg, secondChunk) {
  secondChunk.root = firstMsg.value.content.root || firstMsg.key
  secondChunk.branch = [firstMsg.key]
  return secondChunk
}

test('recps', async t => {
  const ssb = Server({ tribes: true })

  const publish = SplitPublish(ssb, splitter, { afterPublish })

  const largeContent = {
    type: 'post',
    text: [
      ...new Array(4000).fill('a'),
      ...new Array(5000).fill('b'),
      ...new Array(4000).fill('c')
    ].join(''),
    root: null,
    recps: [ssb.id]
  }

  const msgs = await publish(largeContent)

  const expected = [
    {
      type: 'post',
      text: new Array(4000).fill('a').join(''),
      root: null,
      recps: [ssb.id]
    },
    {
      type: 'post',
      text: new Array(4000).fill('b').join(''),
      root: msgs[0].key,
      branch: [msgs[0].key],
      recps: [ssb.id]
    },
    {
      type: 'post',
      text: [
        ...new Array(1000).fill('b'),
        ...new Array(4000).fill('c')
      ].join(''),
      root: msgs[0].key,
      // NOTE this is what the root *should* be, but our afterPublish does not have access to content, so is defaulting to incorrect content
      // root: msgs[1].key,
      branch: [msgs[1].key],
      recps: [ssb.id]
    }
  ]

  t.deepLooseEqual(msgs.map(m => m.value.content), expected, 'three encrypted messages are published correctly')

  t.end()
  ssb.close()
})
